package main

import (
	// s "gitlab.com/Alvoras/tower/internal/shell"
	"fmt"
	// "github.com/davecgh/go-spew/spew"
	"gitlab.com/Alvoras/tower/internal/context"
	_ "gitlab.com/Alvoras/tower/internal/modules"
	_ "gitlab.com/Alvoras/tower/internal/payloads"
)

func main() {
	// pl, _ := context.Ctx.Payloads.Get("reverse_tcp")
	// fmt.Println(pl.GetParam("LHOST"))
	// fmt.Println(pl.GetAttrString("Name"))
	// fmt.Println(pl.GetAttrSliceString("Aliases"))
	// val, _ := pl.GetAttrMapString("Params")
	// fmt.Println(val["LPORT"])
	// fmt.Println(pl.SetAttrSliceString("Aliases", []string{"abcd", "efgh"}))
	// fmt.Println(pl.GetAttrSliceString("Aliases"))
	// pl.SetAttrMapString("Params", map[string]string{"ABCD": "EFGH"})
	// val, _ = pl.GetAttrMapString("Params")
	// fmt.Println(val["ABCD"])
	// return
	fmt.Println("Loaded", context.LoadedPlugins, "templates")
	context.Ctx.Shell.Run()
	context.Ctx.Shell.Close()
}
