package main

import (
  "testing"
  "gitlab.com/Alvoras/tower/internal/context"
  "gitlab.com/Alvoras/tower/internal/payloads"
)

var (
	payload = &context.PayloadConfig{
		Name:         "reverse_tcp",
		Platform:     payloads.Platform.Windows,
		Techno:       payloads.Techno.Powershell,
		TemplatePath: "reverse_tcp",
		Params: map[string]string{
			"LHOST": "auto",
			"LPORT": "4321",
		},
		Aliases: []string{},
	}
)

func TestHasParam(t *testing.T)  {
  testArg := "LHOST"
  if payload.HasParam(testArg) == false {
    t.Errorf("HasParam failed (arg = %s)", testArg)
  }
}

// pl, _ := context.Ctx.Payloads.Get("reverse_tcp")
// fmt.Println("Loaded", context.LoadedPlugins, "plugins")
// fmt.Println(pl.GetParam("LHOST"))
// fmt.Println(pl.GetAttrString("Name"))
// fmt.Println(pl.GetAttrSliceString("Aliases"))
// val, _ := pl.GetAttrMapString("Params")
// fmt.Println(val["LPORT"])
// fmt.Println(pl.SetAttrSliceString("Aliases", []string{"abcd", "efgh"}))
// fmt.Println(pl.GetAttrSliceString("Aliases"))
// pl.SetAttrMapString("Params", map[string]string{"ABCD": "EFGH"})
// val, _ = pl.GetAttrMapString("Params")
// fmt.Println(val["ABCD"])
