package utils

import (
	"fmt"
	"gitlab.com/Alvoras/tower/internal/context"
	// "gitlab.com/Alvoras/tower/internal/dao"
	"ishell_fork"
	"net"
	"net/http"
	"reflect"
	// "strconv"
	"strings"
	// "time"
)

func StatusExists(status int) bool {
	res := false

	if len(http.StatusText(status)) > 0 {
		res = true
	}

	return res
}

// func SetPointer(field *reflect.Value, value string, destType string) bool {
// 	ret := true
//
// 	switch destType {
// 	case "int64":
// 		parsed, err := strconv.ParseInt(value, 10, 64)
// 		if err != nil {
// 			fmt.Println(fmt.Sprintf("Failed to detect the type (detected : %s)", destType))
// 			ret = false
// 		} else {
// 			field.SetInt(parsed)
// 		}
//
// 	case "time.Duration":
// 		parsed, err := time.ParseDuration(value)
// 		if err != nil {
// 			fmt.Println(fmt.Sprintf("Failed to detect the type (detected : %s)", destType))
// 			ret = false
// 		} else {
// 			parsedVal := reflect.ValueOf(parsed)
// 			field.Set(parsedVal)
// 		}
//
// 	case "string":
// 		field.Set(reflect.ValueOf(value))
//
// 	default:
// 		fmt.Println("Error while setting the value")
// 		ret = false
// 	}
//
// 	return ret
// }

// func FieldExists(in interface{}, fieldName string) bool {
// 	st := reflect.ValueOf(in)
//
// 	if st.Kind() == reflect.Ptr {
// 		st = st.Elem()
// 	}
//
// 	if st.Kind() == reflect.Struct {
// 		field := st.FieldByName(fieldName)
// 		return field.IsValid()
// 	}
//
// 	return false
// }

// func FindStructByName(list map[string]interface{}, name string) (interface{}, error) {
// 	if st, ok := list[name]; ok {
// 		return st, nil
// 	}
//
// 	for _, st := range context.Ctx.ConfigList {
// 		inVal := reflect.ValueOf(st)
// 		if inVal.Kind() == reflect.Ptr {
// 			inVal = inVal.Elem()
// 		}
//
// 		aliases := inVal.FieldByName("Meta").FieldByName("Aliases").Interface().([]string)
//
// 		for _, alias := range aliases {
// 			if strings.EqualFold(alias, name) {
// 				return st, nil
// 			}
// 		}
// 	}
//
// 	return nil, fmt.Errorf("Unknown item : %s", name)
// }

func PrintInterface(in interface{}, level int) {
	itfVal := reflect.ValueOf(in)
	if itfVal.IsNil() {
		fmt.Println("Nil array found")
		return
	}

	if itfVal.Kind() == reflect.Ptr {
		itfVal = itfVal.Elem()
	}

	itfType := itfVal.Type()

	if level == 0 {
		fmt.Println(itfVal.FieldByName("Meta").FieldByName("DisplayName").Interface().(string), ":")
	}

	for idx := 0; idx < itfVal.NumField(); idx++ {
		field := itfVal.Field(idx)

		if field.Kind() == reflect.Interface {
			if field.IsNil() {
				continue
			}
			level++
			PrintInterface(field.Elem().Interface(), level)
		} else if field.Kind() != reflect.Struct {
			fmt.Printf("\t%s = %v <%s>\n", itfType.Field(idx).Name, field, field.Type())
		}
	}
}

func SetFromMode(c *ishell.Context) {
	name := context.Ctx.State.CurrentMode
	field := c.Args[0]
	value := c.Args[1]

	SetValueFromName(name, field, value, c)
}

func SetBasic(c *ishell.Context) {
	line := c.Args[0]
	split := strings.SplitN(line, ":", 2)

	if len(split) < 2 {
		c.Println("Usage : set <item>:<field> <value>")
		return
	}

	name := split[0]
	field := split[1]
	value := c.Args[1]

	SetValueFromName(name, field, value, c)
}

func SetValueFromName(name, field, value string, c *ishell.Context) {
	var st interface{}
	// st, err := FindStructByName(context.Ctx.ConfigList, name)
	//
	// if err != nil {
	// 	c.Println(err)
	// }

	switch name {
	case "pl":
		fallthrough
	case "payload":
		st = context.Ctx.Payloads.Selected
		if err := st.(*context.PayloadConfig).SetParam(field, value); err != nil {
			c.Println(fmt.Sprintf("Unknown field : %s", field))
		}

	case "stg":
		fallthrough
	case "stager":
		st = context.Ctx.StagerConfig
		if err := st.(*context.StagerConfig).SetParam(field, value); err != nil {
			c.Println(fmt.Sprintf("Unknown field : %s", field))
		}

	case "srv":
		fallthrough
	case "server":
		st = context.Ctx.SrvConfig
		if err := st.(*context.ServerConfig).SetParam(field, value); err != nil {
			c.Println(fmt.Sprintf("Unknown field : %s", field))
		}

	}

	// fmt.Println(st)

	// if err := st.SetParam(field, value); err != nil {
	// 	c.Println(fmt.Sprintf("Unknown field : %s", field))
	// } else {
	// 	c.Println(fmt.Sprintf("%s => %s", field, value))
	// }

	// if FieldExists(st, field) {
	// 	fieldPointer := reflect.ValueOf(st).Elem().FieldByName(field)
	//
	// 	if ok := SetPointer(&fieldPointer, value, fieldPointer.Type().String()); ok {
	// 		c.Println(fmt.Sprintf("%s => %v", field, fieldPointer))
	// 	}
	// } else {
	// }
}

func GetInterfacesIPs() map[string]string {
	list := make(map[string]string)
	ifaces, err := net.Interfaces()
	if err != nil {
		fmt.Println("Failed to get interfaces")
		return list
	}
	for _, iface := range ifaces {
		addrs, err := iface.Addrs()
		if err != nil {
			fmt.Println("Failed to get IP for interface ", iface.Name)
			return list
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip.To4() != nil {
				list[iface.Name] = ip.String()
			}
		}
	}

	return list
}

func ChooseNIC(c *ishell.Context) string {
	IPList := GetInterfacesIPs()
	choiceList := []string{}
	displayList := []string{}

	for key, val := range IPList {
		choiceList = append(choiceList, key)
		displayList = append(displayList, key+" ("+val+")")
	}

	choice := c.MultiChoice(displayList, "Choose a NIC :")

	return IPList[choiceList[choice]]
}

func AutoSelectNIC(c *ishell.Context) string {
	ip := ""

	IPList := GetInterfacesIPs()
	primaryNIC := []string{
		"eth0",
		"enp7s0",
	}

	for _, nic := range primaryNIC {
		if val, ok := IPList[nic]; ok {
			c.Println("Auto selected " + nic)
			ip = val
			break
		}
	}

	return ip
}
