package dao

import (
	"fmt"
	"reflect"
)

func (setter *GetterSetter) SetParam(field, value string) error {
	if setter.HasParam(field) {
		setter.Params[field] = value
		fmt.Printf("%s => %s\n", field, value)
	}

	return fmt.Errorf("\"%s\" does not exist", field)
}

func (gtrstr *GetterSetter) SetAttrString(field, value string) error {
	if !gtrstr.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(gtrstr)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)
	fieldVal.SetString(value)

	fmt.Printf("%s => %s\n", field, value)
	return nil
}

func (gtrstr *GetterSetter) SetAttrSliceString(field string, slice []string) error {
	if !gtrstr.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(gtrstr)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)

	sliceVal := reflect.ValueOf(slice)
	fieldVal.Set(sliceVal)

	fmt.Printf("%s set\n", field)
	return nil
}

func (gtrstr *GetterSetter) SetAttrMapString(field string, mapString map[string]string) error {
	if !gtrstr.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(gtrstr)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)

	mapVal := reflect.ValueOf(mapString)
	fieldVal.Set(mapVal)

	fmt.Printf("%s set\n", field)
	return nil
}
