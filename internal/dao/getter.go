package dao

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"reflect"
)

func (gtrstr *GetterSetter) HasParam(name string) bool {
	spew.Dump(gtrstr)
	if _, ok := gtrstr.Params[name]; ok {
		return true
	}

	return false
}

func (gtrstr *GetterSetter) GetParam(field string) (string, error) {
	if gtrstr.HasParam(field) {
		return gtrstr.Params[field], nil
	}

	return "", fmt.Errorf("\"%s\" does not exist", field)
}

func (gtrstr *GetterSetter) HasAttr(name string) bool {
	val := reflect.ValueOf(gtrstr)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)

	return fieldVal.Kind() != reflect.Invalid
}

func (gtrstr *GetterSetter) GetAttrString(field string) (string, error) {
	if !gtrstr.HasAttr(field) {
		return "", fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(gtrstr)
	fmt.Println("val", val)
	valPtr := val.Elem()
	fmt.Println("valPtr", valPtr)
	fieldVal := valPtr.FieldByName(field)
	fmt.Println("field val", fieldVal)
	return fieldVal.Interface().(string), nil
}

func (gtrstr *GetterSetter) GetAttrSliceString(name string) ([]string, error) {
	if !gtrstr.HasAttr(name) {
		return []string{}, fmt.Errorf("\"%s\" does not exist", name)
	}

	val := reflect.ValueOf(gtrstr)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)
	return fieldVal.Interface().([]string), nil
}

func (gtrstr *GetterSetter) GetAliases(name string) []string {
	aliases, _ := gtrstr.GetAttrSliceString("Aliases")

	return aliases
}

// HS ?
func (gtrstr *GetterSetter) GetAttrMapString(name string) (map[string]string, error) {
	if !gtrstr.HasAttr(name) {
		return map[string]string{}, fmt.Errorf("\"%s\" does not exist", name)
	}

	val := reflect.ValueOf(gtrstr)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)
	return fieldVal.Interface().(map[string]string), nil
}
