package dao

type GetterSetter struct {
	PayloadConfig
}

type PayloadConfig struct {
	Name         string
	TemplatePath string
	Platform     string
	Techno       map[string]string
	Params       map[string]string
	Aliases      []string
}
