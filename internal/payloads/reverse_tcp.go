package payloads

import (
	"gitlab.com/Alvoras/tower/internal/context"
)

func init() {
	payload := &context.PayloadConfig{
		Name:         "reverse_tcp",
		Platform:     Platform.Windows,
		Techno:       Techno.Powershell,
		TemplatePath: "assets/payload/templates/windows/powershell/reverse_tcp.ps1",
		Params: map[string]string{
			"LHOST": "auto",
			"LPORT": "4321",
		},
		Meta: context.ConfigMetadata{
			DisplayName: "Reverse TCP",
			Aliases:     []string{},
		},
		Aliases: []string{},
	}

	context.Ctx.Payloads.Register(payload)
}
