package payloads

type PlatformConst struct {
	Windows string
	Linux   string
}

type Technology struct {
	Powershell map[string]string
}

var (
	Platform = PlatformConst{
		Windows: "windows",
		Linux:   "linux",
	}

	Techno = Technology{
		Powershell: map[string]string{"powershell": "ps1"},
	}
)
