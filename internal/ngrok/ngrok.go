package ngrok

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os/exec"
)

func GetNgrokPublicAddress() (string, error) {
	type Res struct {
		PublicURL string `json:"public_url"`
	}

	st := Res{}
	// command_line is the default tunnel name
	res, err := http.Get("http://127.0.0.1:4040/api/tunnels/command_line")
	if err != nil {
		return "", err
	}

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	json.Unmarshal(responseData, &st)
	publicURL := st.PublicURL

	return publicURL, nil
}

func OpenNgrokTunnel(port string, killCh chan bool) bool {
	cmd := exec.Command("ngrok", "http", port)
	var out bytes.Buffer
	cmd.Stdout = &out
	go cmd.Run()

	select {
	case <-killCh:
		fmt.Println("Killing ngrok server...")
		cmd.Process.Kill()
		fmt.Println("Ngrok server killed")
		return true
	}
}
