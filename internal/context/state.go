package context

import (
	"fmt"
)

type GlobalState struct {
	Modes       map[string]bool
	CurrentMode string
	Locked      bool
}

var (
	State *GlobalState
)

func (state *GlobalState) CheckRequire(mode string) bool {
	if !state.Check(mode) {
		fmt.Printf("Not in %s mode\n", mode)
		return false
	} else {
		return true
	}
}

func (state *GlobalState) Reset() {
	for mode := range state.Modes {
		state.Modes[mode] = false
	}

	state.Locked = false
	state.CurrentMode = ""
}

func (state *GlobalState) ModeExists(name string) bool {
	ret := false

	for mode := range state.Modes {
		if mode == name {
			ret = true
			break
		}
	}

	return ret
}

func (state *GlobalState) SetMode(name string) {
	state.CurrentMode = name
	state.Modes[name] = true
	state.Locked = true
}

func (state *GlobalState) Check(name string) bool {
	ret := false

	if ok := state.Modes[name] == true; ok {
		ret = true
	}

	return ret
}
