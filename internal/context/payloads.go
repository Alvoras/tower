package context

import (
	"fmt"
	// "gitlab.com/Alvoras/tower/internal/dao"
	"reflect"
)

var (
	LoadedPlugins = 0
)

type PayloadConfig struct {
	Name         string
	TemplatePath string
	Platform     string
	Techno       map[string]string
	Params       map[string]string
	Aliases      []string
	Meta         ConfigMetadata
	// dao.GetterSetter
}

type PayloadList struct {
	ByName   map[string]*PayloadConfig
	ByTechno map[string][]*PayloadConfig
}

type PayloadsContext struct {
	List     *PayloadList
	Selected *PayloadConfig
}

func InitPayloadDefault() *PayloadConfig {
	payloadCfg := &PayloadConfig{
		Name:         "Default",
		TemplatePath: "",
		Aliases:      []string{"pl"},
		Params:       map[string]string{},
		Meta: ConfigMetadata{
			DisplayName: "Default payload",
			Aliases:     []string{},
		},
	}

	return payloadCfg
}

/** PayloadContext **/

func (plctx *PayloadsContext) Register(config *PayloadConfig) {
	techno := config.GetTechnoName()

	plctx.List.ByName[config.Name] = config
	plctx.List.ByTechno[techno] = append(plctx.List.ByTechno[techno], config)

	LoadedPlugins++
}

func (plcfg *PayloadsContext) Get(name string) (*PayloadConfig, error) {
	if val, ok := plcfg.List.ByName[name]; ok {
		return val, nil
	}

	return nil, fmt.Errorf("\"%s\" does not exist", name)
}

/** PayloadConfig **/

func (plcfg *PayloadConfig) GetTechnoName() string {
	var key string

	for key, _ = range plcfg.Techno {
	}

	return key
}

func (plcfg *PayloadConfig) GetTechnoExt() string {
	var ext string

	for _, ext = range plcfg.Techno {
	}

	return ext
}

func (plcfg *PayloadConfig) HasParam(name string) bool {
	// spew.Dump(plcfg)
	if _, ok := plcfg.Params[name]; ok {
		return true
	}

	return false
}

func (plcfg *PayloadConfig) GetParam(field string) (string, error) {
	if plcfg.HasParam(field) {
		return plcfg.Params[field], nil
	}

	return "", fmt.Errorf("\"%s\" does not exist", field)
}

func (plcfg *PayloadConfig) HasAttr(name string) bool {
	val := reflect.ValueOf(plcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)

	return fieldVal.Kind() != reflect.Invalid
}

func (plcfg *PayloadConfig) GetAttrString(field string) (string, error) {
	if !plcfg.HasAttr(field) {
		return "", fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(plcfg)
	fmt.Println("val", val)
	valPtr := val.Elem()
	fmt.Println("valPtr", valPtr)
	fieldVal := valPtr.FieldByName(field)
	fmt.Println("field val", fieldVal)
	return fieldVal.Interface().(string), nil
}

func (plcfg *PayloadConfig) GetAttrSliceString(name string) ([]string, error) {
	if !plcfg.HasAttr(name) {
		return []string{}, fmt.Errorf("\"%s\" does not exist", name)
	}

	val := reflect.ValueOf(plcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)
	return fieldVal.Interface().([]string), nil
}

func (plcfg *PayloadConfig) GetAliases(name string) []string {
	aliases, _ := plcfg.GetAttrSliceString("Aliases")

	return aliases
}

// HS ?
func (plcfg *PayloadConfig) GetAttrMapString(name string) (map[string]string, error) {
	if !plcfg.HasAttr(name) {
		return map[string]string{}, fmt.Errorf("\"%s\" does not exist", name)
	}

	val := reflect.ValueOf(plcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)
	return fieldVal.Interface().(map[string]string), nil
}

func (setter *PayloadConfig) SetParam(field, value string) error {
	if setter.HasParam(field) {
		setter.Params[field] = value
		fmt.Printf("%s => %s\n", field, value)
		return nil
	}

	return fmt.Errorf("\"%s\" does not exist", field)
}

func (plcfg *PayloadConfig) SetAttrString(field, value string) error {
	if !plcfg.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(plcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)
	fieldVal.SetString(value)

	fmt.Printf("%s => %s\n", field, value)
	return nil
}

func (plcfg *PayloadConfig) SetAttrSliceString(field string, slice []string) error {
	if !plcfg.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(plcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)

	sliceVal := reflect.ValueOf(slice)
	fieldVal.Set(sliceVal)

	fmt.Printf("%s set\n", field)
	return nil
}

func (plcfg *PayloadConfig) SetAttrMapString(field string, mapString map[string]string) error {
	if !plcfg.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(plcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)

	mapVal := reflect.ValueOf(mapString)
	fieldVal.Set(mapVal)

	fmt.Printf("%s set\n", field)
	return nil
}
