package context

import (
	"context"
	"errors"
	"ishell_fork"
	"net/http"
)

type ConfigMetadata struct {
	DisplayName string
	Aliases     []string
}

type Context struct {
	Shell     *ishell.Shell
	Payloads  *PayloadsContext
	State     *GlobalState
	Server    *http.Server
	SrvConfig *ServerConfig
	// ConfigList     map[string]interface{}
	StagerConfig   *StagerConfig
	DefaultPayload *PayloadConfig
	NgrokKillCh    chan bool
}

var (
	Ctx *Context
)

func init() {
	Ctx = &Context{
		Shell: ishell.New(),
		Payloads: &PayloadsContext{
			List: &PayloadList{
				ByName:   make(map[string]*PayloadConfig),
				ByTechno: make(map[string][]*PayloadConfig),
			},
			Selected: InitPayloadDefault(),
		},
		State: &GlobalState{
			Modes: map[string]bool{
				"stager":  false,
				"payload": false,
				"server":  false,
			},
			Locked: false,
		},
		SrvConfig:    InitServerDefault(),
		StagerConfig: InitStagerDefault(),
		// ConfigList:  make(map[string]interface{}),
		NgrokKillCh: make(chan bool),
	}
	Ctx.StagerConfig.Params["Endpoint"] = Ctx.SrvConfig.Params["Endpoint"]

	// Ctx.ConfigList["server"] = Ctx.SrvConfig
	// Ctx.ConfigList["payload"] = Ctx.DefaultPayload
	// Ctx.ConfigList["stager"] = Ctx.StagerConfig

	Ctx.Shell.SetHomeHistoryPath(".tower_history")
	Ctx.Shell.Println("Tower v0.10")
}

func (ctx *Context) GetConfigByName(name string) interface{} {
	var conf interface{}

	switch name {
	case "pl":
		fallthrough
	case "payload":
		conf = ctx.Payloads.Selected
	case "stg":
		fallthrough
	case "stager":
		conf = ctx.StagerConfig
	case "srv":
		fallthrough
	case "server":
		conf = ctx.SrvConfig
	default:
		return nil
	}

	return conf
}

func (ctx *Context) Register(cmd *ishell.Cmd) {
	ctx.Shell.AddCmd(cmd)
}

func (ctx *Context) RegisterAll(cmdList []*ishell.Cmd) {
	for _, cmd := range cmdList {
		ctx.Register(cmd)
	}
}

func (Ctx *Context) ShutdownServer() error {
	ctx, cancel := context.WithTimeout(context.Background(), Ctx.SrvConfig.ShutdownTimeout)
	defer cancel()

	if Ctx.SrvConfig.Params["ForwardNgrok"] == "true" {
		Ctx.NgrokKillCh <- true
	}

	if Ctx.Server != nil {
		return Ctx.Server.Shutdown(ctx)
	} else {
		return errors.New("The server is not running")
	}
}
