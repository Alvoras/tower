package context

import (
	"fmt"
	// "gitlab.com/Alvoras/tower/internal/dao"
	"os/exec"
	"reflect"
	"time"
)

type ServerConfig struct {
	WriteTimeout    time.Duration
	ReadTimeout     time.Duration
	IdleTimeout     time.Duration
	ShutdownTimeout time.Duration
	Params          map[string]string
	Meta            ConfigMetadata
	// dao.GetterSetter
}

func InitServerDefault() *ServerConfig {
	srvConfig := &ServerConfig{
		WriteTimeout:    time.Second * 15,
		ReadTimeout:     time.Second * 15,
		IdleTimeout:     time.Second * 60,
		ShutdownTimeout: time.Second * 5,
		Params: map[string]string{
			"Port":         "8080",
			"Endpoint":     "file",
			"ForwardNgrok": "true",
		},
		Meta: ConfigMetadata{
			DisplayName: "Server configuration",
			Aliases:     []string{"srv"},
		},
	}

	if _, err := exec.LookPath("ngrok"); err != nil {
		fmt.Println("ngrok executable not found")
		fmt.Println("This option is disabled")
		srvConfig.SetParam("ForwardNgrok", "false")
	}

	return srvConfig
}

func (srvcfg *ServerConfig) HasParam(name string) bool {
	// spew.Dump(srvcfg)
	if _, ok := srvcfg.Params[name]; ok {
		return true
	}

	return false
}

func (srvcfg *ServerConfig) GetParam(field string) (string, error) {
	if srvcfg.HasParam(field) {
		return srvcfg.Params[field], nil
	}

	return "", fmt.Errorf("\"%s\" does not exist", field)
}

func (srvcfg *ServerConfig) HasAttr(name string) bool {
	val := reflect.ValueOf(srvcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)

	return fieldVal.Kind() != reflect.Invalid
}

func (srvcfg *ServerConfig) GetAttrString(field string) (string, error) {
	if !srvcfg.HasAttr(field) {
		return "", fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(srvcfg)
	fmt.Println("val", val)
	valPtr := val.Elem()
	fmt.Println("valPtr", valPtr)
	fieldVal := valPtr.FieldByName(field)
	fmt.Println("field val", fieldVal)
	return fieldVal.Interface().(string), nil
}

func (srvcfg *ServerConfig) GetAttrSliceString(name string) ([]string, error) {
	if !srvcfg.HasAttr(name) {
		return []string{}, fmt.Errorf("\"%s\" does not exist", name)
	}

	val := reflect.ValueOf(srvcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)
	return fieldVal.Interface().([]string), nil
}

func (srvcfg *ServerConfig) GetAliases(name string) []string {
	aliases, _ := srvcfg.GetAttrSliceString("Aliases")

	return aliases
}

// HS ?
func (srvcfg *ServerConfig) GetAttrMapString(name string) (map[string]string, error) {
	if !srvcfg.HasAttr(name) {
		return map[string]string{}, fmt.Errorf("\"%s\" does not exist", name)
	}

	val := reflect.ValueOf(srvcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)
	return fieldVal.Interface().(map[string]string), nil
}

func (setter *ServerConfig) SetParam(field, value string) error {
	if setter.HasParam(field) {
		setter.Params[field] = value
		fmt.Printf("%s => %s\n", field, value)
		return nil
	}

	return fmt.Errorf("\"%s\" does not exist", field)
}

func (srvcfg *ServerConfig) SetAttrString(field, value string) error {
	if !srvcfg.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(srvcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)
	fieldVal.SetString(value)

	fmt.Printf("%s => %s\n", field, value)
	return nil
}

func (srvcfg *ServerConfig) SetAttrSliceString(field string, slice []string) error {
	if !srvcfg.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(srvcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)

	sliceVal := reflect.ValueOf(slice)
	fieldVal.Set(sliceVal)

	fmt.Printf("%s set\n", field)
	return nil
}

func (srvcfg *ServerConfig) SetAttrMapString(field string, mapString map[string]string) error {
	if !srvcfg.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(srvcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)

	mapVal := reflect.ValueOf(mapString)
	fieldVal.Set(mapVal)

	fmt.Printf("%s set\n", field)
	return nil
}
