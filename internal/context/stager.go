package context

import (
	"fmt"
	"path/filepath"
	"reflect"
)

type MCUConfig struct {
	Package string
	Arch    string
	Board   string
	CPU     string
}

type StagerConfig struct {
	Name         string
	TemplatePath string
	Params       map[string]string
	Meta         ConfigMetadata
}

func InitStagerDefault() *StagerConfig {
	// Default board options
	sketch, _ := filepath.Abs("assets/stager/patched.ino")
	firmwareTmpl, _ := filepath.Abs("assets/stager/templates/windows/stager.ino")
	// comport := "/dev/ttyACM0"
	comport := "auto"
	pkg := "SparkFun"
	arch := "avr"
	board := "promicro"
	cpu := "16MHzatmega32U4"

	stagerCfg := &StagerConfig{
		Name: "Stager",
		// TemplatePath: firmwareTmpl,
		TemplatePath: firmwareTmpl,
		Params: map[string]string{
			"RHOST":     "auto",
			"RPORT":     "-1",
			"Package":   pkg,
			"Arch":      arch,
			"Board":     board,
			"CPU":       cpu,
			"Sketch":    sketch,
			"COMPort":   comport,
			"BoardLine": fmt.Sprintf("%s:%s:%s:cpu=%s", pkg, arch, board, cpu),
		},
		Meta: ConfigMetadata{
			DisplayName: "Stager",
			Aliases:     []string{"stager", "drp", "dropper"},
		},
	}

	return stagerCfg
}

func (stgcfg *StagerConfig) HasParam(name string) bool {
	if _, ok := stgcfg.Params[name]; ok {
		return true
	}

	return false
}

func (stgcfg *StagerConfig) GetParam(field string) (string, error) {
	if stgcfg.HasParam(field) {
		return stgcfg.Params[field], nil
	}

	return "", fmt.Errorf("\"%s\" does not exist", field)
}

func (stgcfg *StagerConfig) HasAttr(name string) bool {
	val := reflect.ValueOf(stgcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)

	return fieldVal.Kind() != reflect.Invalid
}

func (stgcfg *StagerConfig) GetAttrString(field string) (string, error) {
	if !stgcfg.HasAttr(field) {
		return "", fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(stgcfg)
	fmt.Println("val", val)
	valPtr := val.Elem()
	fmt.Println("valPtr", valPtr)
	fieldVal := valPtr.FieldByName(field)
	fmt.Println("field val", fieldVal)
	return fieldVal.Interface().(string), nil
}

func (stgcfg *StagerConfig) GetAttrSliceString(name string) ([]string, error) {
	if !stgcfg.HasAttr(name) {
		return []string{}, fmt.Errorf("\"%s\" does not exist", name)
	}

	val := reflect.ValueOf(stgcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)
	return fieldVal.Interface().([]string), nil
}

func (stgcfg *StagerConfig) GetAliases(name string) []string {
	aliases, _ := stgcfg.GetAttrSliceString("Aliases")

	return aliases
}

// HS ?
func (stgcfg *StagerConfig) GetAttrMapString(name string) (map[string]string, error) {
	if !stgcfg.HasAttr(name) {
		return map[string]string{}, fmt.Errorf("\"%s\" does not exist", name)
	}

	val := reflect.ValueOf(stgcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(name)
	return fieldVal.Interface().(map[string]string), nil
}

func (setter *StagerConfig) SetParam(field, value string) error {
	if setter.HasParam(field) {
		setter.Params[field] = value
		fmt.Printf("%s => %s\n", field, value)
		return nil
	}

	return fmt.Errorf("\"%s\" does not exist", field)
}

func (stgcfg *StagerConfig) SetAttrString(field, value string) error {
	if !stgcfg.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(stgcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)
	fieldVal.SetString(value)

	fmt.Printf("%s => %s\n", field, value)
	return nil
}

func (stgcfg *StagerConfig) SetAttrSliceString(field string, slice []string) error {
	if !stgcfg.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(stgcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)

	sliceVal := reflect.ValueOf(slice)
	fieldVal.Set(sliceVal)

	fmt.Printf("%s set\n", field)
	return nil
}

func (stgcfg *StagerConfig) SetAttrMapString(field string, mapString map[string]string) error {
	if !stgcfg.HasAttr(field) {
		return fmt.Errorf("\"%s\" does not exist", field)
	}

	val := reflect.ValueOf(stgcfg)
	valPtr := val.Elem()
	fieldVal := valPtr.FieldByName(field)

	mapVal := reflect.ValueOf(mapString)
	fieldVal.Set(mapVal)

	fmt.Printf("%s set\n", field)
	return nil
}
