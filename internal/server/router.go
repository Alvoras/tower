package server

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/Alvoras/tower/internal/context"
	"net/http"
	"path/filepath"
)

var (
	Router *mux.Router
)

func init() {
	Router = mux.NewRouter()
	Router.HandleFunc("/status/{status}", Status).Methods("GET", "HEAD")
	// Router.HandleFunc("/nc{bits}", Nc).Methods("GET")
}

func EndpointFile(w http.ResponseWriter, r *http.Request) {
	path, _ := filepath.Abs(fmt.Sprintf("assets/live/%s", context.Ctx.SrvConfig.Params["Endpoint"]))
	http.ServeFile(w, r, path)
}

func SetEndpoint(ep string) {
	Router.HandleFunc(fmt.Sprintf("/%s", ep), EndpointFile).Methods("GET")
}
