package server

import (
	// "fmt"
	"net/http"
	// "path/filepath"
	"strconv"

	"gitlab.com/Alvoras/tower/internal/utils"

	"github.com/gorilla/mux"
)

func Status(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	status, err := strconv.Atoi(vars["status"])

	if err == nil && utils.StatusExists(status) {
		w.WriteHeader(status)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

//
// func Nc(w http.ResponseWriter, r *http.Request) {
// 	vars := mux.Vars(r)
// 	bits := vars["bits"]
// 	path, _ := filepath.Abs(fmt.Sprintf("assets/nc%s.exe", bits))
// 	http.ServeFile(w, r, path)
// }
