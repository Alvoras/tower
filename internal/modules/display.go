package modules

import (
	"fmt"
	"gitlab.com/Alvoras/tower/internal/context"
	"gitlab.com/Alvoras/tower/internal/utils"
	"ishell_fork"
)

func init() {
	cmdList := []*ishell.Cmd{
		&ishell.Cmd{
			Name:    "params",
			Aliases: []string{"config"},
			Help:    "Print the configuration for the current mode",
			Func:    Params,
		},
		&ishell.Cmd{
			Name: "listpl",
			Help: "Display a list of the avalaible payloads",
			Func: Listpl,
		},
		&ishell.Cmd{
			Name: "print",
			Help: "Print the given configuration",
			Func: PrintConfig,
		},
		&ishell.Cmd{
			Name: "ppl",
			Help: "Print the current payload. Alias for \"print payload\"",
			Func: PrintPayload,
		},
		&ishell.Cmd{
			Name: "pstg",
			Help: "Print the current stager configuration. Alias for \"print stager\"",
			Func: PrintStager,
		},
		&ishell.Cmd{
			Name: "psrv",
			Help: "Print the current stager configuration. Alias for \"print server\"",
			Func: PrintServer,
		},
	}

	context.Ctx.RegisterAll(cmdList)
}

func Params(c *ishell.Context) {
	if !context.Ctx.State.Locked {
		c.Println("No mode selected")
		return
	}

	confName := context.Ctx.State.CurrentMode

	if st := context.Ctx.GetConfigByName(confName); st == nil {
		fmt.Printf("Unknown item : %s\n", confName)
	} else {
		utils.PrintInterface(st, 0)
	}

}

func PrintConfig(c *ishell.Context) {
	if len(c.Args) == 0 {
		c.Println("Usage print <item>")
		return
	}
	confName := c.Args[0]

	if st := context.Ctx.GetConfigByName(confName); st == nil {
		fmt.Printf("Unknown item : %s\n", confName)
	} else {
		utils.PrintInterface(st, 0)
	}
}

func PrintPayload(c *ishell.Context) {
	utils.PrintInterface(context.Ctx.GetConfigByName("payload"), 0)
}

func PrintStager(c *ishell.Context) {
	utils.PrintInterface(context.Ctx.GetConfigByName("stager"), 0)
}

func PrintServer(c *ishell.Context) {
	utils.PrintInterface(context.Ctx.GetConfigByName("server"), 0)
}

func Listpl(c *ishell.Context) {
	for name, _ := range context.Ctx.Payloads.List.ByName {
		c.Printf("%s :\n", name)
	}
}
