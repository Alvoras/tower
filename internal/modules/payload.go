package modules

import (
	"bytes"
	"fmt"
	"gitlab.com/Alvoras/tower/internal/context"
	"gitlab.com/Alvoras/tower/internal/utils"
	"ishell_fork"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

var (
	reqModePayload = "payload"
)

func init() {
	cmdList := []*ishell.Cmd{
		&ishell.Cmd{
			Name:    "select",
			Aliases: []string{"use"},
			Help:    "Select the payload to serve",
			Func:    Select,
		},
		&ishell.Cmd{
			Name: "serve",
			Help: "Make the configured payload available",
			Func: Serve,
		},
		&ishell.Cmd{
			Name: "retire",
			Help: "Remove the served payload",
			Func: Retire,
		},
	}

	context.Ctx.RegisterAll(cmdList)
}

func Select(c *ishell.Context) {
	if !context.Ctx.State.CheckRequire(reqModePayload) {
		return
	}

	var split []string
	if len(c.Args) < 1 {
		c.Println("Usage : select <payload>")
		return
	}

	// Split with os.PathSeparator
	// split[0] => first dimension
	// split[1] => array key
	if strings.Contains(c.Args[0], string(os.PathSeparator)) {
		split = strings.SplitN(c.Args[0], string(os.PathSeparator), 3)
	} else {
		c.Printf("Unknown payload : %s\n", c.Args[0])
		return
	}

	if len(split) < 3 {
		c.Printf("Unknown payload : %s\n", c.Args[0])
		return
	}

	// category, lang, name := split[:]
	platform := split[0]
	techno := split[1]
	name := split[2]

	if _, ok := context.Ctx.Payloads.List.ByTechno[techno]; ok {
		for _, candidate := range context.Ctx.Payloads.List.ByTechno[techno] {
			fullname := fmt.Sprintf("%s.%s", candidate.Name, candidate.GetTechnoExt())

			if fullname == name &&
				candidate.Platform == platform &&
				candidate.GetTechnoName() == techno {
				context.Ctx.Payloads.Selected = candidate
				break
			}
		}
		c.Printf("Payload => %s\n", c.Args[0])
	} else {
		c.Printf("Unknown payload : %s\n", c.Args[0])
	}
}

func Serve(c *ishell.Context) {
	if !context.Ctx.State.CheckRequire(reqModePayload) {
		return
	}

	if payloadLHOST, ok := context.Ctx.Payloads.Selected.Params["LHOST"]; ok {
		ip := ""

		switch payloadLHOST {
		case "manual":
			ip = utils.ChooseNIC(c)
		case "auto":
			ip = utils.AutoSelectNIC(c)
		default:
			if strings.Contains(payloadLHOST, "auto::") {
				IPList := utils.GetInterfacesIPs()
				iface := strings.Replace(payloadLHOST, "auto::", "", 1)
				if val, ok := IPList[iface]; ok {
					ip = val
				}
			}
		}

		if len(ip) != 0 {
			utils.SetValueFromName("payload", "LHOST", ip, c)
		} else {
			ip = utils.ChooseNIC(c)

			if len(ip) == 0 {
				c.Println("Failed to select an IP")
			} else {
				utils.SetValueFromName("payload", "LHOST", ip, c)
			}
		}
	}

	liveFilepath, err := filepath.Abs(fmt.Sprintf("assets/live/%s", context.Ctx.SrvConfig.Params["Endpoint"]))

	if err != nil {
		c.Println(err)
		return
	}

	var patched bytes.Buffer
	params := context.Ctx.Payloads.Selected.Params

	if params == nil {
		c.Println("No payload selected")
		return
	}

	// tmplPathAttr, err := context.Ctx.Payloads.Selected.GetAttrString("TemplatePath")
	tmplPathAttr := context.Ctx.Payloads.Selected.TemplatePath

	if err != nil {
		c.Println(err)
		return
	} else if tmplPathAttr == "" {
		c.Println("TemplatePath can't be empty")
		return
	}

	tmplPath, err := filepath.Abs(tmplPathAttr)

	if err != nil {
		c.Println(err)
		return
	}

	os.Create(liveFilepath)

	liveFile, err := os.OpenFile(liveFilepath, os.O_RDWR, 0644)
	if err != nil {
		c.Println(err)
		return
	}

	defer liveFile.Close()

	tmpl := template.New("template")

	tmpl = template.Must(tmpl.ParseFiles(
		tmplPath,
	),
	)

	if err := tmpl.ExecuteTemplate(&patched, "template", params); err != nil {
		c.Println("Failed to execute the template :", err)
	}

	patchedStr := strings.TrimLeft(patched.String(), "\n")

	liveFile.WriteString(patchedStr)
	c.Printf("Payload created and loaded at the /%s endpoint\n", context.Ctx.SrvConfig.Params["Endpoint"])
}

func Retire(c *ishell.Context) {
	if !context.Ctx.State.CheckRequire(reqModePayload) {
		return
	}
	liveFilepath, _ := filepath.Abs(fmt.Sprintf("assets/live/%s", context.Ctx.SrvConfig.Params["Endpoint"]))
	os.Truncate(liveFilepath, 0)
}
