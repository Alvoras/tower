package modules

import (
	"fmt"
	"gitlab.com/Alvoras/tower/internal/context"
	"gitlab.com/Alvoras/tower/internal/ngrok"
	"gitlab.com/Alvoras/tower/internal/server"
	"gitlab.com/Alvoras/tower/internal/utils"
	"ishell_fork"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"time"
)

var (
	reqModeServer = "server"
)

func init() {
	cmdList := []*ishell.Cmd{
		&ishell.Cmd{
			Name: "spawn",
			Help: "Spawn the server",
			Aliases: []string{
				"start",
			},
			Func: Spawn,
		},
		&ishell.Cmd{
			Name: "stop",
			Help: "Stop the server",
			Func: Stop,
		},
		&ishell.Cmd{
			Name: "restart",
			Help: "Restart the server",
			Func: Restart,
		},
	}

	context.Ctx.RegisterAll(cmdList)
}

func Spawn(c *ishell.Context) {
	if !context.Ctx.State.CheckRequire(reqModeServer) {
		return
	}

	server.SetEndpoint(context.Ctx.SrvConfig.Params["Endpoint"])

	context.Ctx.Server = &http.Server{
		Addr:         fmt.Sprintf(":%s", context.Ctx.SrvConfig.Params["Port"]),
		WriteTimeout: context.Ctx.SrvConfig.WriteTimeout,
		ReadTimeout:  context.Ctx.SrvConfig.ReadTimeout,
		IdleTimeout:  context.Ctx.SrvConfig.IdleTimeout,
		Handler:      server.Router,
	}

	c.Println("Starting the server...")
	go func() {
		// Print all errors except server closed as it is raised when we're shutting down the server
		if err := context.Ctx.Server.ListenAndServe(); err != http.ErrServerClosed {
			c.Println("Failed to start the server :", err)
		}
	}()

	if context.Ctx.SrvConfig.Params["ForwardNgrok"] == "true" {
		go ngrok.OpenNgrokTunnel(context.Ctx.SrvConfig.Params["Port"], context.Ctx.NgrokKillCh)
		time.Sleep(2 * time.Second)

		publicURL, _ := ngrok.GetNgrokPublicAddress()

		re, err := regexp.Compile(`http[s]://`)
		if err != nil {
			c.Println("Failed to compile regex")
			c.Println("Check the ngrok URL and remove http[s] if present")
		}
		publicURL = re.ReplaceAllString(publicURL, "")

		utils.SetValueFromName("stager", "RHOST", publicURL, c)
		utils.SetValueFromName("stager", "RPORT", "80", c)
	}

	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		context.Ctx.ShutdownServer()
	}()
}

func Stop(c *ishell.Context) {
	if !context.Ctx.State.CheckRequire(reqModeServer) {
		return
	}

	c.Println("Sending stop signal to the server...")

	if err := context.Ctx.ShutdownServer(); err != nil {
		c.Printf("Failed to terminate the server : %s\n", err)
	} else {
		context.Ctx.Server = nil
		c.Println("Server has been terminated")
	}
}

func Restart(c *ishell.Context) {
	if !context.Ctx.State.CheckRequire(reqModeServer) {
		return
	}

	Stop(c)
	Spawn(c)
}
