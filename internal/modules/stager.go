package modules

import (
	"bytes"
	"errors"
	"github.com/fsnotify/fsnotify"
	"gitlab.com/Alvoras/tower/internal/context"
	"os/signal"
	// "gitlab.com/Alvoras/tower/internal/utils"
	"ishell_fork"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"
	"time"
)

var (
	reqModeStager = "stager"
)

func init() {
	cmdList := []*ishell.Cmd{
		&ishell.Cmd{
			Name:    "patch",
			Aliases: []string{"make"},
			Help:    "Patch the firmware template",
			Func:    Patch,
		},
		&ishell.Cmd{
			Name: "flash",
			Help: "Flashes the firmware to the microcontroller",
			Func: Flash,
		},
	}

	context.Ctx.RegisterAll(cmdList)
}

func Patch(c *ishell.Context) {
	if !context.Ctx.State.CheckRequire(reqModeStager) {
		return
	}

	var patched bytes.Buffer

	params := context.Ctx.StagerConfig.Params
	templPath := context.Ctx.StagerConfig.TemplatePath
	patchedFilepath, _ := filepath.Abs("assets/stager/patched.ino")
	// patchedTmpl, _ := filepath.Abs("assets/stager/templates/windows/stager.ino")
	patchedTmpl, err := filepath.Abs(templPath)
	if err != nil {
		c.Println("Invalid template path [" + templPath + "]")
		return
	}

	os.Create(patchedFilepath)

	patchedFile, _ := os.OpenFile(patchedFilepath, os.O_RDWR, 0644)
	defer patchedFile.Close()

	tmpl := template.New("template")

	tmpl = template.Must(tmpl.ParseFiles(
		patchedTmpl,
	),
	)

	if err := tmpl.ExecuteTemplate(&patched, "template", params); err != nil {
		c.Println("Failed to patch the stager's firmware :", err)
	}

	patchedStr := strings.TrimLeft(patched.String(), "\n")

	patchedFile.WriteString(patchedStr)

	c.Printf("Stager's firmware file patched at %s\n", patchedFilepath)
}

func waitForDevice() (string, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return "", err
	}
	defer watcher.Close()

	if err := watcher.Add("/dev/"); err != nil {
		return "", err
	}

	timeout := time.NewTicker(10 * time.Second)

	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)

	for {
		select {
		case ev := <-watcher.Events:
			if ev.Op == fsnotify.Create && strings.Contains(ev.Name, "tty") {
				return ev.Name, nil
			}
		case err := <-watcher.Errors:
			return "", err
		case <-timeout.C:
			timeout.Stop()
			return "", errors.New("Device lookup timeout")
		case <-sigint:
			return "", errors.New("Interrupted by user")
		}
	}
}

func Flash(c *ishell.Context) {
	if !context.Ctx.State.CheckRequire(reqModeStager) {
		return
	}

	args := []string{"--upload", "--port", "--board"}
	params := context.Ctx.StagerConfig.Params

	if params["COMPort"] == "auto" {
		c.Println("Waiting for a new device to be plugged...")
		port, err := waitForDevice()
		if err != nil {
			c.Println(err)
			return
		}

		c.Printf("Found %s\n", port)

		params["COMPort"] = port
	}

	// Run flash board
	cmd := exec.Command("/usr/local/bin/arduino", args[0], params["Sketch"], args[1], params["COMPort"], args[2], params["BoardLine"])
	c.ProgressBar().Indeterminate(true)
	c.ProgressBar().Start()
	err := cmd.Run()
	c.ProgressBar().Stop()

	if err != nil {
		c.Println(err)
		return
	}

	c.Println("Flash OK")
}
