package modules

import (
	"gitlab.com/Alvoras/tower/internal/context"
	"gitlab.com/Alvoras/tower/internal/utils"
	"ishell_fork"
)

func init() {
	cmdList := []*ishell.Cmd{
		&ishell.Cmd{
			Name:    "params",
			Aliases: []string{"config"},
			Help:    "Print the configuration for the current mode",
			Func:    Params,
		},
		&ishell.Cmd{
			Name: "set",
			Help: "Set the given element to the given value",
			Func: Set,
		},
		&ishell.Cmd{
			Name: "setpl",
			Help: "Alias of set payload:<field> value",
			Func: SetPayloadCLI,
		},
		&ishell.Cmd{
			Name: "setstg",
			Help: "Alias of set stager:<field> value",
			Func: SetStagerCLI,
		},
	}

	context.Ctx.RegisterAll(cmdList)
}

func Set(c *ishell.Context) {
	if len(c.Args) < 2 {
		c.Println("Usage : set <item>:<field> <value>")
		return
	}

	if context.Ctx.State.Locked {
		utils.SetFromMode(c)
	} else {
		utils.SetBasic(c)
	}
}

func SetPayloadCLI(c *ishell.Context) {
	SetPayload(c, c.Args[0], c.Args[1])
}

func SetPayload(c *ishell.Context, field, value string) {
	context.Ctx.Payloads.Selected.SetParam(field, value)
	// st := context.Ctx.ConfigList["payload"].(*configs.PayloadConfig).Params
	//
	// if st == nil {
	// 	c.Println("No payload selected. Type \"showpl\" to display a list of the available payloads")
	// 	return
	// }
	//
	// // line := strings.Join(c.Args, " ")
	// // field := c.Args[0]
	// // value := c.Args[1]
	//
	// if utils.FieldExists(st, field) {
	// 	fieldPointer := reflect.ValueOf(st).Elem().FieldByName(field)
	//
	// 	if ok := utils.SetPointer(&fieldPointer, value, fieldPointer.Type().String()); ok {
	// 		c.Println(fmt.Sprintf("%s => %v", field, fieldPointer))
	// 	}
	// } else {
	// 	c.Println(fmt.Sprintf("Unknown field : %s", field))
	// }
}

func SetStagerCLI(c *ishell.Context) {
	SetStager(c, c.Args[0], c.Args[1])
}

func SetStager(c *ishell.Context, field, value string) {
	context.Ctx.StagerConfig.SetParam(field, value)
	// st := context.Ctx.ConfigList["stager"].(*configs.PayloadConfig).Params

	// line := strings.Join(c.Args, " ")
	// field := c.Args[0]
	// value := c.Args[1]

	// if utils.FieldExists(st, field) {
	// 	fieldPointer := reflect.ValueOf(st).Elem().FieldByName(field)
	//
	// 	if ok := utils.SetPointer(&fieldPointer, value, fieldPointer.Type().String()); ok {
	// 		c.Println(fmt.Sprintf("%s => %v", field, fieldPointer))
	// 	}
	// } else {
	// 	c.Println(fmt.Sprintf("Unknown field : %s", field))
	// }

}
