package modules

import (
	"fmt"
	"gitlab.com/Alvoras/tower/internal/context"
	"io/ioutil"
	"ishell_fork"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
)

func init() {
	cmd := &ishell.Cmd{
		Name: "script",
		Help: "Trigger the given script",
		Func: Script,
	}

	context.Ctx.Register(cmd)
}

func Script(c *ishell.Context) {
	if len(c.Args) < 1 {
		c.Println("Usage : script <path>")
		return
	}
	basePath, err := filepath.Abs("assets/scripts")
	if err != nil {
		c.Println("Script folder not found")
		return
	}

	path, err := filepath.Abs(fmt.Sprintf("%s/%s", basePath, c.Args[0]))
	if err != nil {
		c.Printf("Script not found at %s\n", path)
		return
	}

	script, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Printf("Failed to read %s\n", path)
	}

	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)

	cmds := strings.Split(string(script), "\n")
	for _, line := range cmds {
		// Ignore comments and empty lines
		if strings.HasPrefix(line, "#") || len(line) == 0 {
			continue
		}
		prepLine := strings.Split(line, " ")
		select {
		case <-sigint:
			c.Println("Script execution terminated by user")
			return
		default:
			c.Println(prepLine)
			context.Ctx.Shell.Process(prepLine...)
		}
	}
}
