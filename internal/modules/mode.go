package modules

import (
	"fmt"
	"gitlab.com/Alvoras/tower/internal/context"
	"ishell_fork"
	"os"
	"time"
)

func init() {
	cmdList := []*ishell.Cmd{
		&ishell.Cmd{
			Name: "mode",
			Help: "Enter the specified mode if available",
			Func: Mode,
			Completer: func([]string) []string {
				var modes []string

				for mode := range context.Ctx.State.Modes {
					modes = append(modes, mode)
				}

				return modes
			},
		},
		&ishell.Cmd{
			Name:    "exit",
			Aliases: []string{"back"},
			Help:    "Exit the specified mode if currently selected",
			Func:    Exit,
		},
	}

	context.Ctx.RegisterAll(cmdList)
}

func Mode(c *ishell.Context) {
	mode := c.Args[0]
	if context.Ctx.State.Locked {
		if context.Ctx.State.Modes[mode] == true {
			return
		}
		Exit(c)
	}

	if context.Ctx.State.ModeExists(mode) {
		c.Printf("Entering %s mode\n", mode)
		context.Ctx.State.SetMode(mode)

		c.SetPrompt(fmt.Sprintf("(%s)> ", mode))
	} else {
		c.Printf("Unknown mode : %s\n", mode)
	}
}

func Exit(c *ishell.Context) {
	if !context.Ctx.State.Locked {
		context.Ctx.ShutdownServer()
		time.Sleep(3 * time.Second)
		os.Exit(0)
	}

	c.Printf("Exiting %s mode\n", context.Ctx.State.CurrentMode)

	context.Ctx.State.Reset()

	c.SetPrompt(">>> ")
}
