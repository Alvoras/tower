package modules

import (
	"github.com/fatih/color"
	"gitlab.com/Alvoras/tower/internal/context"
	"ishell_fork"
	"os"
	"path/filepath"
	"strings"
)

func init() {
	cmd := &ishell.Cmd{
		Name:    "search",
		Aliases: []string{"lookup"},
		Help:    "Print all the matching file names in payloads and scripts",
		Func:    Search,
	}

	context.Ctx.Register(cmd)
}

func printResult(c *ishell.Context, results []string) {
	for _, el := range results {
		c.Print("+ ")
		c.Println(el)
	}
}

func Search(c *ishell.Context) {
	if len(c.Args) == 0 {
		c.Println("Usage : search <string>")
		return
	}
	bold := color.New(color.FgWhite, color.Bold).SprintFunc()
	needle := c.Args[0]
	var root string
	var paths []string

	walkFn := func(path string, f os.FileInfo, err error) error {
		if !f.IsDir() {
			strippedPath := strings.Replace(path, root+"/", "", -1)

			if strings.Contains(strippedPath, needle) {
				boldPath := strings.Replace(strippedPath, needle, bold(needle), -1)
				paths = append(paths, boldPath)
			}
		}
		return err
	}

	root = "assets/scripts"

	if err := filepath.Walk(root, walkFn); err != nil {
		c.Println(err)
	}
	c.Println("Scripts :")
	printResult(c, paths)
	c.Println()

	paths = []string{}

	root = "assets/payload/templates"

	if err := filepath.Walk(root, walkFn); err != nil {
		c.Println(err)
	}
	c.Println("Payloads :")
	printResult(c, paths)
}
