package configs

import (
// "time"
)

// type PayloadConfig struct {
// 	Name         string
// 	TemplatePath string
// 	Params       interface{}
// 	Meta         ConfigMetadata
// }

// type ReverseTCP struct {
// 	LPORT int64
// 	LHOST string
// }

// type StagerParams struct {
// 	TemplatePath string
// 	LPORT        int64
// 	LHOST        string
// 	BoardLine    string
// 	COMPort      string
// 	Package      string
// 	Arch         string
// 	Board        string
// 	CPU          string
// 	Sketch       string
// 	Endpoint     string
// }

// var (
// 	PlList         = make(map[string]map[string]interface{})
// 	PSReverseTCP   *PayloadConfig
// 	BashReverseTCP *PayloadConfig
// )
//
// func init() {
// 	PSReverseTCP = &PayloadConfig{
// 		Name:         "ps_reverse_tcp",
// 		TemplatePath: "assets/payload/templates/windows/powershell/reverse_tcp.ps1",
// 		Params: &ReverseTCP{
// 			LHOST: "",
// 			LPORT: -1,
// 		},
// 		Meta: ConfigMetadata{
// 			DisplayName: "Powershell TCP reverse shell",
// 			Aliases:     []string{"psrtcp", "pl"},
// 		},
// 	}
//
// 	BashReverseTCP = &PayloadConfig{
// 		Name:         "bash_reverse_tcp",
// 		TemplatePath: "assets/payload/templates/linux/bash/reverse_tcp.sh",
// 		Params: &ReverseTCP{
// 			LHOST: "",
// 			LPORT: -1,
// 		},
// 		Meta: ConfigMetadata{
// 			DisplayName: "Bash TCP reverse shell",
// 			Aliases:     []string{"shrtcp", "pl"},
// 		},
// 	}
//
// 	PlList["powershell"] = make(map[string]interface{})
// 	PlList["bash"] = make(map[string]interface{})
// 	PlList["powershell"]["reverse_tcp"] = PSReverseTCP
// 	PlList["bash"]["reverse_tcp"] = BashReverseTCP
// }
